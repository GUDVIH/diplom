// search all slider slides
var allSlides = document.querySelectorAll('.slider__slide');

// on click slider buttons
var allNavs = document.querySelectorAll('.slider__button');

// show first slide
allSlides[0].style.width = '100%';
allSlides[0].style.opacity = '1';

// next slide
function nextSlide() {
    clearTimeout(timer);
    for (var i = 0; i < allSlides.length; i++) {
        if (allSlides[i].style.width == "100%") {
            for(var j = 0; j < allNavs.length; j++) {
                allNavs[j].classList.remove('slider__active');
                allNavs[i].classList.add('slider__active');
            }
            if (i == allSlides.length - 1) {
                allSlides[i].style.width = "0px";
                allSlides[0].style.width = "100%";
                allSlides[i].style.opacity = "0";
                allSlides[0].style.opacity = "1";
                autoSlider();
                break;
            }
            allSlides[i].style.width = "0px";
            allSlides[i + 1].style.width = "100%";
            allSlides[i].style.opacity = "0";
            allSlides[i + 1].style.opacity = "1";
            autoSlider();
            break;
        }
    }
}

// auto slider
var timer = 0;
function autoSlider() {
    timer = setTimeout(nextSlide, 3000);
}
autoSlider();


for (var i = 0; i < allNavs.length; i++) {
    allNavs[i].addEventListener('click', function (event) {
        clearTimeout(timer);
        for (var x = 0; x < allNavs.length; x++) {
            if (allNavs[x] == event.currentTarget) {
                allSlides[x].style.width = "100%";
                allSlides[x].style.opacity = "1";
                autoSlider();
            } else {
                allSlides[x].style.width = "0px";
                allSlides[x].style.opacity = "0";
            }
        }
        for(var j = 0; j < allNavs.length; j++) {
            allNavs[j].classList.remove('slider__active');
            event.srcElement.classList.add('slider__active');
        }
    });
}