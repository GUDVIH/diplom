// search all slider slides
var allSlidesFour = document.querySelectorAll('.testimonials__slide');
// on click slider buttons
var allNavsFour = document.querySelectorAll('.testimonials__btn');

// show first slide
allSlidesFour[0].style.width = '100%';
allSlidesFour[0].style.opacity = '1';

// next slide
function nextSlideFour() {
    clearTimeout(timerFour);
    for (var i = 0; i < allSlidesFour.length; i++) {
        if (allSlidesFour[i].style.width == "100%") {
            for(var j = 0; j < allNavsFour.length; j++) {
                allNavsFour[j].classList.remove('testimonials__active');
                allNavsFour[i].classList.add('testimonials__active');
            }
            if (i == allSlidesFour.length - 1) {
                allSlidesFour[i].style.width = "0px";
                allSlidesFour[0].style.width = "100%";
                allSlidesFour[i].style.opacity = "0";
                allSlidesFour[0].style.opacity = "1";
                autoSliderFour();
                break;
            }
            allSlidesFour[i].style.width = "0px";
            allSlidesFour[i + 1].style.width = "100%";
            allSlidesFour[i].style.opacity = "0";
            allSlidesFour[i + 1].style.opacity = "1";
            autoSliderFour();
            break;
        }
    }
}

// auto slider
var timerFour = 0;
function autoSliderFour() {
    timerFour = setTimeout(nextSlideFour, 3000);
}
autoSliderFour();

for (var i = 0; i < allNavsFour.length; i++) {
    allNavsFour[i].addEventListener('click', function (event) {
        clearTimeout(timerFour);
        for (var x = 0; x < allNavsFour.length; x++) {
            if (allNavsFour[x] == event.currentTarget) {
                allSlidesFour[x].style.width = "100%";
                allSlidesFour[x].style.opacity = "1";
                autoSliderFour();
            } else {
                allSlidesFour[x].style.width = "0px";
                allSlidesFour[x].style.opacity = "0";
            }
        }
        for(var j = 0; j < allNavsFour.length; j++) {
            allNavsFour[j].classList.remove('testimonials__active');
            event.srcElement.classList.add('testimonials__active');
        }
    });
}