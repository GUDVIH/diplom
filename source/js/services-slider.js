// search all slider slides
var allSlidesSecond = document.querySelectorAll('.slider__slide2');

// on click slider buttons
var allNavsSecond = document.querySelectorAll('.services__btn');

// show first slide
allSlidesSecond[0].style.height = '270px';
allSlidesSecond[0].style.opacity = '1';

// next slide
function nextSlideSecond() {
    clearTimeout(timerSecond);
    for (var i = 0; i < allSlidesSecond.length; i++) {
        if (allSlidesSecond[i].style.height == "270px") {
            for(var j = 0; j < allNavsSecond.length; j++) {
                allNavsSecond[j].classList.remove('services__active');
                allNavsSecond[i].classList.add('services__active');
            }
            if (i == allSlidesSecond.length - 1) {
                allSlidesSecond[i].style.height = "0px";
                allSlidesSecond[0].style.height = "270px";
                allSlidesSecond[i].style.opacity = "0";
                allSlidesSecond[0].style.opacity = "1";
                autoSliderSecond();
                break;
            }
            allSlidesSecond[i].style.height = "0px";
            allSlidesSecond[i + 1].style.height = "270px";
            allSlidesSecond[i].style.opacity = "0";
            allSlidesSecond[i + 1].style.opacity = "1";
            autoSliderSecond();
            break;
        }
    }
}

// auto slider
var timerSecond = 0;
function autoSliderSecond() {
    timerSecond = setTimeout(nextSlideSecond, 3000);
}
autoSliderSecond();


for (var i = 0; i < allNavsSecond.length; i++) {
    allNavsSecond[i].addEventListener('click', function (event) {
        clearTimeout(timerSecond);
        for (var x = 0; x < allNavsSecond.length; x++) {
            if (allNavsSecond[x] == event.currentTarget) {
                allSlidesSecond[x].style.height = "270px";
                allSlidesSecond[x].style.opacity = "1";
                autoSliderSecond();
            } else {
                allSlidesSecond[x].style.height = "0px";
                allSlidesSecond[x].style.opacity = "0";
            }
        }
        for(var j = 0; j < allNavsSecond.length; j++) {
            allNavsSecond[j].classList.remove('services__active');
            event.srcElement.classList.add('services__active');
        }
    });
}